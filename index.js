var http = require('http');

/*

/marci - toate marcile
    GET,POST,PUT
/marci/:id - toate informatiile despre marca respectiva
    GET,PATCH,DELETE
/marci/:id/modele - toate modelele marcii cu id-ul dat
    GET,POST,PUT
/marci/:id/modele/:id - toate informatiile despre modelul marcii mentionate
    GET,PATCH,DELETE
/marci/:id/modele/:id/versiuni - toate versiunile
    GET,POST,PUT
/marci/:id/modele/:id/versiuni/:id - informatiile despre o versiune
    GET,PATCH,DELETE


*/

var db = require('./db.js')

const config = {
    host: "localhost",
    user: "andim",
    password: "andim",
    database: "cc"};
let conn = new db(config);



function metode_colectii(conn, res, req, numeTabel, idPrec='{}') {
    // get
    var campuriIntroduse;
    switch(numeTabel) {
        case "marci":
            campuriIntroduse = ["nume", "descriere", "link"]; break;
        case "modele":
            campuriIntroduse = ["nume", "intrerupt"]; break;
        default:
            campuriIntroduse = ["an_fabricatie", "capacitate", "putere", "culoare", "pret"];
    }

    if(req.method == "GET") {
        var whereVersiuni = idPrec.hasOwnProperty('id_model') ? JSON.stringify({id_model : idPrec['id_model'], id_marca: idPrec['id_marca']}) : JSON.stringify({id_marca : idPrec['id_marca']});
        conn.select_query("versiuni v", whereVersiuni, ['v.id','v.id_model','v.an_fabricatie', 'v.capacitate', 'v.putere', 'v.culoare', 'v.pret'], JSON.stringify({modele : ["mo", "mo.id", "v.id_model"]}), (numeTabel == "versiuni") ? "" : "id_model")
            .then(function(versiuni) {
                if(numeTabel == "versiuni") {
                    res.writeHead(200, {"Content-Type":"application/json"});
                    res.end(JSON.stringify(versiuni));
                } else {
                    conn.select_query("modele", idPrec.hasOwnProperty('id_marca') ? JSON.stringify({id_marca : idPrec['id_marca']}) : "{}", ['id', 'id_marca', 'nume', 'intrerupt'])
                        .then(function(modele) {
                            var i = -1, prev = -1;
                            if(versiuni.length > 0) {
                                versiuni.forEach(function(v) {
                                    if(v['id_model'] != prev) { i++; prev = v['id_model']; }
                                    while(modele[i]['id'] != v['id_model']) i++;
                                    if(modele[i].hasOwnProperty('versiuni')) {
                                        modele[i]['versiuni'].push(v);
                                    } else
                                        modele[i]['versiuni'] = [v];
                                    });
                            }

                            if(numeTabel == "modele") {
                                res.writeHead(200, {"Content-Type":"application/json"});
                                res.end(JSON.stringify(modele));
                            } else {
                                if(modele.length > 0)
                                    modele.sort((a,b) => parseInt(a.id_marca - b.id_marca));
                                conn.select_query("marci", "{}", ['id', 'nume', 'descriere', 'link'])
                                    .then(function(marci) {
                                        //console.log(marci);
                                        //console.log(modele);
                                        var i = -1, prev = -1;
                                        if(modele.length > 0) {
                                            modele.forEach(function(m) {
                                                if(m['id_marca'] != prev) { i++; prev = m['id_marca']; }
                                                while(marci[i]['id'] != m['id_marca']) i++;
                                                if(marci[i].hasOwnProperty('modele')) {
                                                    marci[i]['modele'].push(m);
                                                } else
                                                    marci[i]['modele'] = [m];
                                            });    
                                        }

                                        res.writeHead(200, {"Content-Type":"application/json"});
                                        res.end(JSON.stringify(marci));
                                    });
                            }
                        });
                }
            });    
    } else if(req.method == "POST" || req.method == "PUT") {
        if(req.headers['content-type'] != 'application/json') {
            res.writeHead(415, {"Content-Type": "application/json"});
            res.end(JSON.stringify({error:"unsupported media type", message:"informatiile trebuie transmise in format JSON"}));
        } else {
            req.on('data', (data) =>{
                let stringData = data.toString();
                try {
                    let jsonData = JSON.parse(stringData);

                    var conditie = Object.keys(jsonData).length != campuriIntroduse.length;

                    if(!conditie) {
                        for(var i = 0; i < campuriIntroduse.length; i++) {
                            if(!jsonData.hasOwnProperty(campuriIntroduse[i])) {
                                conditie = true;
                                break;
                            }
                        }
                    }

                    if(numeTabel == "modele") jsonData['id_marca'] = idPrec['id_marca'];
                    else if(numeTabel == "versiuni") jsonData['id_model'] = idPrec['id_model'];

                    if(conditie) {
                        res.writeHead(400, {"Content-Type": "application/json"});
                        res.end(JSON.stringify({error:"bad request", message:`resursa de tip ${numeTabel} trebuie sa aiba DOAR ${campuriIntroduse.join(',')}.`}));
                    } else {
                        // check if data is already there
                        var identificator = (numeTabel == "versiuni") ? jsonData : {nume:jsonData['nume']};
                        conn.select_query(numeTabel, JSON.stringify(identificator), ['id'])
                            .then(function(rezultat) {
                                if(rezultat.length > 0) {
                                    if(req.method == "POST") {
                                        res.writeHead(409, {"Content-Type": "application/json"});
                                        res.end(JSON.stringify({error:"conflict", message:`exista aceasta resursa de tip ${numeTabel} cu id-ul ${rezultat[0]['id']}`}))
                                    } else {
                                        conn.update_query(numeTabel, stringData, rezultat[0]['id'])
                                            .then(function(rezultat) {
                                                if(rezultat['status'] == "ok") {
                                                    res.writeHead(204);
                                                    res.end();
                                                } else {
                                                    res.writeHead(400, {"Content-Type": "application/json"});
                                                    res.end(JSON.stringify({error:"bad request", message:`resursa de tip ${numeTabel} trebuie sa aiba DOAR ${campuriIntroduse.join(',')}.`}));
                                                }
                                            })
                                    }
                                } else {
                                    conn.insert_query(numeTabel, JSON.stringify(jsonData))
                                    .then(function(rezultat) {
                                        if(rezultat['status'] != "error") {
                                            var location;
                                            switch(numeTabel) {
                                                case "marci":
                                                    location = `/marci/${rezultat['status']}`; break;
                                                case "modele":
                                                    location = `/marci/${idPrec['id_marca']}/modele/${rezultat['status']}`; break;
                                                default:
                                                    location = `/marci/${idPrec['id_marca']}/modele/${idPrec['id_model']}/versiuni/${rezultat['status']}`;
                                            }
                                            res.writeHead(201, {"Location":location});
                                            res.end();
                                        } else {
                                            res.writeHead(400, {"Content-Type": "application/json"});
                                            res.end(JSON.stringify({error:"bad request", message:`resursa de tip ${numeTabel} trebuie sa aiba DOAR ${campuriIntroduse.join(',')}.`}));
                                        }
                                    });
                                }
                            });
                    }
                } catch (error) {
                    res.writeHead(400, {"Content-Type":"application/json"});
                    res.end(JSON.stringify({error: "bad request", message: "mesajul introdus nu este de tip JSON"}));
                }
                
                
                
            
                //res.end();
            });
        }
    } else if(req.method != "HEAD") {
        res.writeHead(405, {"Content-Type": "application/json"});
        res.end(JSON.stringify({error:"method not allowed", message:"metodele acceptate sunt GET,POST,PUT"}));
    }
}

function metode_resurse(conn, res, req, numeTabel) {
    var campuriIntroduse;
    switch(numeTabel) {
        case "marci":
            campuriIntroduse = ["nume", "descriere", "link"]; break;
        case "modele":
            campuriIntroduse = ["nume", "intrerupt"]; break;
        default:
            campuriIntroduse = ["an_fabricatie", "capacitate", "putere", "culoare", "pret"];
    }
    var bucatele = req.url.split('/').slice(1);

    if(req.method == "GET") {
        var whereVersiuni = {"mo.id_marca" : bucatele[1]};
        if(bucatele.length > 3) whereVersiuni['id_model'] = bucatele[3];
        if(bucatele.length > 5) whereVersiuni['v.id'] = bucatele[5];
        conn.select_query("versiuni v", JSON.stringify(whereVersiuni), ['v.id','v.id_model','v.an_fabricatie', 'v.capacitate', 'v.putere', 'v.culoare', 'v.pret'], JSON.stringify({modele : ["mo", "mo.id", "v.id_model"]}), (numeTabel == "versiuni") ? "" : "id_model")
            .then(function(versiuni) {
                if(numeTabel == "versiuni") {
                    res.writeHead(200, {"Content-Type":"application/json"});
                    res.end(JSON.stringify(versiuni[0]));
                } else {
                    var whereModele = {};
                    if(bucatele.length > 1) whereModele['id_marca'] = bucatele[1];
                    if(bucatele.length > 3) whereModele['id'] = bucatele[3];

                    conn.select_query("modele", JSON.stringify(whereModele), ['id', 'id_marca', 'nume', 'intrerupt'])
                        .then(function(modele) {
                            var i = -1, prev = -1;
                            if(versiuni.length > 0) {
                                versiuni.forEach(function(v) {
                                    if(v['id_model'] != prev) { i++; prev = v['id_model']; }
                                    while(modele[i]['id'] != v['id_model']) i++;
                                    if(modele[i].hasOwnProperty('versiuni')) {
                                        modele[i]['versiuni'].push(v);
                                    } else
                                        modele[i]['versiuni'] = [v];
                                    });
                            }

                            if(numeTabel == "modele") {
                                res.writeHead(200, {"Content-Type":"application/json"});
                                res.end(JSON.stringify(modele[0]));
                            } else {
                                if(modele.length > 0)
                                    modele.sort((a,b) => parseInt(a.id_marca - b.id_marca));
                                conn.select_query("marci", JSON.stringify({id:bucatele[1]}), ['id', 'nume', 'descriere', 'link'])
                                    .then(function(marci) {

                                        var i = -1, prev = -1;
                                        if(modele.length > 0) {

                                            modele.forEach(function(m) {
                                                if(m['id_marca'] != prev) { i++; prev = m['id_marca']; }
                                                while(marci[i]['id'] != m['id_marca']) i++;
                                                if(marci[i].hasOwnProperty('modele')) {
                                                    marci[i]['modele'].push(m);
                                                } else
                                                    marci[i]['modele'] = [m];
                                            });    
                                        }

                                        res.writeHead(200, {"Content-Type":"application/json"});
                                        res.end(JSON.stringify(marci[0]));
                                    });
                            }
                            
                        });
                }
            }); 
    } else if(req.method == "PATCH") {
        if(req.headers['content-type'] != 'application/json') {
            res.writeHead(415, {"Content-Type": "application/json"});
            res.end(JSON.stringify({error:"unsupported media type", message:"informatiile trebuie transmise in format JSON"}));
        } else {
            req.on('data', (data) =>{
                let stringData = data.toString();
                try {
                    let jsonData = JSON.parse(stringData); 

                    if(jsonData.hasOwnProperty('id') || jsonData.hasOwnProperty('created_at') || jsonData.hasOwnProperty('updated_at') || Object.keys(jsonData).length > campuriIntroduse.length) {
                        res.writeHead(400, {"Content-Type": "application/json"});
                        res.end(JSON.stringify({error:"bad request", message:`se pot edita doar campurile ${campuriIntroduse}`}));
                    } else {
                        var conditie = true;
                        if(numeTabel == "versiuni") {
                            for(var i = 0; i < campuriIntroduse.length; i++) {
                                if(!jsonData.hasOwnProperty(campuriIntroduse[i])) {
                                    conditie = false;
                                    break;
                                }
                            }
                        } else conditie = jsonData.hasOwnProperty("nume");

                        if(conditie) {
                            conn.select_query(numeTabel, stringData, ['id'])
                                .then(function(rezultat) {
                                    if(rezultat.length > 0) {
                                        res.writeHead(409, {"Content-Type": "application/json"});
                                        res.end(JSON.stringify({error:"conflict", message:`resursa cu id-ul ${rezultat[0]['id']} are acelasi identificator (in cazul marci si modele, numele, altfel toate campurile sunt identice)`}));
                                    } else {
                                        var id;
                                        switch(numeTabel) {
                                            case "marci": id = bucatele[1]; break;
                                            case "modele": id = bucatele[3]; break;
                                            default : id = bucatele[5];

                                        }
                                        conn.update_query(numeTabel, stringData, id)
                                            .then(function(rezultat) {
                                                if(rezultat['status'] == "ok") {
                                                    res.writeHead(204);
                                                    res.end();
                                                } else {
                                                    res.writeHead(400, {"Content-Type": "application/json"});
                                                    res.end(JSON.stringify({error:"bad request", message:`se pot edita doar campurile ${campuriIntroduse}.`}));
                                                }
                                            });
                                    }
                                });
                        } else {
                            var id;
                            switch(numeTabel) {
                                case "marci": id = bucatele[1]; break;
                                case "modele": id = bucatele[3]; break;
                                default : id = bucatele[5];

                            }
                            conn.update_query(numeTabel, stringData, id)
                                .then(function(rezultat) {
                                    if(rezultat['status'] == "ok") {
                                        res.writeHead(204);
                                        res.end();
                                    } else {
                                        res.writeHead(400, {"Content-Type": "application/json"});
                                        res.end(JSON.stringify({error:"bad request", message:`se pot edita doar campurile ${campuriIntroduse}.`}));
                                    }
                                });
                        }
                    }
                } catch(error) {
                    res.writeHead(400, {"Content-Type":"application/json"});
                    res.end(JSON.stringify({error: "bad request", message: "mesajul introdus nu este de tip JSON"}));
                }
            });
        }
    } else if(req.method == "DELETE") {
        var id;
        switch(numeTabel) {
            case "marci": id = bucatele[1]; break;
            case "modele": id = bucatele[3]; break;
            default : id = bucatele[5];

        }
        conn.delete_query(numeTabel, id)
            .then(function(rezultat) {
                if(rezultat['status'] == "ok") {
                    res.writeHead(200);
                    res.end();
                }
            });
    } else if(req.method != "HEAD") {
        res.writeHead(405, {"Content-Type": "application/json"});
        res.end(JSON.stringify({error:"method not allowed", message:"metodele acceptate sunt GET,PATCH,DELETE"}));
    }
}



var simple_server = http.createServer(function (req, res) {
    var bucatele = req.url.split('/').slice(1);

    if(bucatele.length == 0) {
        res.writeHead(404, {"Content-Type":"application/json"});
        res.end(JSON.stringify({error:"not found", message:"nicio colectie selectata"}));
    } else { // > 0
        if(bucatele[0] != "marci") {
            res.writeHead(404, {"Content-Type":"application/json"});
            res.end(JSON.stringify({error:"not found", message:`colectia '${bucatele[0]}' nu exista (incercati 'marci')`}));
        } else if(bucatele.length == 1) {
            //metode_marci(conn, res, req);
            metode_colectii(conn, res, req, "marci");
        } else { // > 1
            conn.select_query("marci", JSON.stringify({id:bucatele[1]}), ["id"])
                .then(function(result) {
                    if(result.length == 0) {
                        res.writeHead(404, {"Content-Type": "application/json"});
                        res.end(JSON.stringify({error:"not found", message:`marca cu id-ul ${bucatele[1]} nu a fost gasita`}));
                    } else if(bucatele.length == 2) {
                        //metode_marci_id(conn, res, req);
                        metode_resurse(conn, res, req, "marci");
                    } else { // > 2
                        if(bucatele[2] != "modele") {
                            res.writeHead(404, {"Content-Type":"application/json"});
                            res.end(JSON.stringify({error:"not found", message:`o marca nu contine colectia '${bucatele[2]}' (incercati 'modele')`}));
                        } else if(bucatele.length == 3) {
                            metode_colectii(conn, res, req, "modele", {id_marca : bucatele[1]});
                        } else { // > 3
                            conn.select_query("modele", JSON.stringify({id:bucatele[3]}), ["id_marca"])
                                .then(function(result) {
                                    if(result.length == 0) {
                                        res.writeHead(404, {"Content-Type": "application/json"});
                                        res.end(JSON.stringify({error:"not found", message:`modelul cu id-ul ${bucatele[3]} nu a fost gasit`}));
                                    } else if(result[0]["id_marca"] != bucatele[1]) {
                                            res.writeHead(404, {"Content-Type": "application/json"});
                                            res.end(JSON.stringify({error:"not found", message:`modelul cu id-ul ${bucatele[3]} nu apartine marcii cu id-ul ${bucatele[1]}`}));
                                    } else if(bucatele.length == 4) {
                                        metode_resurse(conn, res, req, "modele");
                                    } else { // > 4
                                        if(bucatele[4] != "versiuni") {
                                            res.writeHead(404, {"Content-Type":"application/json"});
                                            res.end(JSON.stringify({error:"not found", message:`un model nu contine colectia '${bucatele[4]}' (incercati 'versiuni')`}));
                                        } else if(bucatele.length == 5) {
                                            metode_colectii(conn, res, req, "versiuni", {id_marca : bucatele[1], id_model: bucatele[3]});
                                        } else { // > 5
                                            conn.select_query("versiuni", JSON.stringify({id:bucatele[5]}), ["id_model"])
                                                .then(function(result) {
                                                    if(result.length == 0) {
                                                        res.writeHead(404, {"Content-Type": "application/json"});
                                                        res.end(JSON.stringify({error:"not found", message:`versiunea cu id-ul ${bucatele[5]} nu a fost gasita`}));
                                                    } else if(result[0]["id_model"] != bucatele[3]) {
                                                            res.writeHead(404, {"Content-Type": "application/json"});
                                                            res.end(JSON.stringify({error:"not found", message:`versiunea cu id-ul ${bucatele[5]} nu apartine marcii cu id-ul ${bucatele[3]}`}));                                                    
                                                    } else if(bucatele.length == 6) {
                                                        metode_resurse(conn, res, req, "versiuni");
                                                    } else { // > 6
                                                        res.writeHead(404, {"Content-Type": "application/json"});
                                                        res.end(JSON.stringify({error:"not found", message:"o resursa a colectiei 'versiuni' nu mai contine alte colectii"}));
                                                    }
                                                });
                                        }

                                    }
                                });
                        }
                    }
                });
        }
    }
});

simple_server.listen(5000);