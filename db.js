
var mysql = require('mysql')

function getCurrentDateTimeString() {
    const date = new Date();
    return date.getFullYear() + '-' +
        (date.getMonth() + 1).toString().padStart(2, '0') + '-' +
        date.getDate().toString().padStart(2, '0') + ' ' +
        date.getHours().toString().padStart(2, '0') + ':' +
        date.getMinutes().toString().padStart(2, '0') + ':' +
        date.getSeconds().toString().padStart(2, '0');
}

class Database {
    constructor( config ) {
        this.connection = mysql.createConnection( config );

        // create table masina

        this.connection.query("CREATE TABLE IF NOT EXISTS marci ( \
            id INT AUTO_INCREMENT PRIMARY KEY, \
            nume VARCHAR(100) NOT NULL, \
            descriere VARCHAR(255) NOT NULL, \
            link VARCHAR(100) NOT NULL, \
            created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP, \
            updated_at TIMESTAMP \
            ) ENGINE=INNODB;");
    
        // create table marca

        this.connection.query("CREATE TABLE IF NOT EXISTS modele ( \
            id INT AUTO_INCREMENT PRIMARY KEY, \
            id_marca INT NOT NULL, \
            nume VARCHAR(100) NOT NULL, \
            intrerupt BOOLEAN DEFAULT false, \
            created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP, \
            updated_at TIMESTAMP, \
            FOREIGN KEY (id_marca) \
            REFERENCES marci(id) \
            ON DELETE CASCADE \
            ) ENGINE=INNODB;");
    
        // create table model
    
        this.connection.query("CREATE TABLE IF NOT EXISTS versiuni ( \
            id INT AUTO_INCREMENT PRIMARY KEY, \
            id_model INT NOT NULL, \
            an_fabricatie INT NOT NULL, \
            capacitate INT NOT NULL, \
            putere INT NOT NULL, \
            culoare VARCHAR(100) NOT NULL, \
            pret INT NOT NULL, \
            created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP, \
            updated_at TIMESTAMP, \
            FOREIGN KEY (id_model) \
            REFERENCES modele(id) \
            ON DELETE CASCADE \
            ) ENGINE=INNODB;");
    }

    select_query(nume_tabel, date="{}", campuri_selectate=['*'], tabele_join = "{}", order_by = '') {
        var jsonData = JSON.parse(date);
        var values = Array();
        var sql = 'SELECT ' + ''.concat(campuri_selectate) + ` FROM ${nume_tabel} `;

        //console.log(sql);
        
        var jsonJoin = JSON.parse(tabele_join);
        Object.keys(jsonJoin).forEach(function(key) {
            let lista = jsonJoin[key];
            sql = sql + `JOIN ${key} ${lista[0]} ON ${lista[1]}=${lista[2]} `
        });

        var first = true;
        Object.keys(jsonData).forEach(function(key) {
            if(jsonData[key].length > 0 && typeof jsonData[key] != "string") {
                // do nothing
            } else {
                values.push(jsonData[key]);
            }
            if(first) {
                var semn = (jsonData[key].length > 0 && typeof jsonData[key] != "string") ? ` IN (${jsonData[key].join(',')})`  : "=? ";
                sql = sql.concat(`WHERE ${key}${semn}`);
                first = false;
            } else
                sql = sql.concat(`AND ${key}=? `);
        });
    
        

        if(order_by != '') {
            sql = sql + ` ORDER BY ${order_by}`;
        }

        sql = sql.concat(';');


        
        return new Promise( (resolve, reject) => {
            this.connection.query(sql, values, (err, rows) => {
                if(err) resolve({error: err});
                else resolve(rows);
            })
        });    
    }

    insert_query(nume_tabel, date) {
        var jsonData = JSON.parse(date);

        var values = Array();
        var sql = `INSERT INTO ${nume_tabel}(`;
        var semneIntrebare = Array();
        //console.log(sql);
        
        var first = true;
        var chei = Object.keys(jsonData);

        sql = sql.concat(chei);
        
        chei.forEach((key) => {
            values.push(jsonData[key]);
            semneIntrebare.push('?');
        });
    
        sql = sql + ') VALUES(';
        sql = sql.concat(semneIntrebare);
        sql = sql + ');';

        return new Promise( (resolve, reject) => {
            this.connection.query(sql, values, (err, rows) => {
                if(err) resolve({status: "error"});
                else resolve({status: rows.insertId});
            })
        });
    }

    update_query(nume_tabel, date, id) {
        var jsonData = JSON.parse(date);

        var values = Array();
        var sql = `UPDATE ${nume_tabel} SET `;
        var setString = Array();
        
        var chei = Object.keys(jsonData);
        
        chei.forEach((key) => {
            setString.push(`${key} = ?`);
            values.push(jsonData[key]);
        });

        setString.push('updated_at = ?');
        values.push(getCurrentDateTimeString());
    
        sql = sql.concat(setString);
        sql = sql + ' WHERE id = ?;';
        values.push(id);

        //console.log(sql);
        //console.log(values);
        
        
        return new Promise( (resolve, reject) => {
            this.connection.query(sql, values, (err, rows) => {
                if(err) resolve({status: err});
                else resolve({status: 'ok'});
            })
        });
    }

    delete_query(nume_tabel, id) {
        var sql = `DELETE FROM ${nume_tabel} WHERE id = ?`;

        return new Promise( (resolve, reject) => {
            this.connection.query(sql, id, (err, rows) => {
                if(err) resolve({status: err});
                resolve({status: 'ok'});
            })
        });
    }

    query( sql, args ) {
        return new Promise( ( resolve, reject ) => {
            this.connection.query( sql, args, ( err, rows ) => {
                if ( err )
                    return reject( err );
                resolve( rows );
            } );
        } );
    }
    close() {
        return new Promise( ( resolve, reject ) => {
            this.connection.end( err => {
                if ( err )
                    return reject( err );
                resolve();
            } );
        } );
    }
}

module.exports = Database